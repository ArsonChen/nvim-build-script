apt-add-repository ppa:neovim-ppa/stable
apt-get update
apt-get install neovim
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
mkdir -p ~/.config/nvim/
pip install neovim
cp ./init.vim ~/.config/nvim
nvim +PlugInstall +qa
